# -*- coding: utf-8 -*-
"""
Created on Fri Mar 25 15:35:51 2022

@author: Desarrollo
"""

import jwt # PyJWT==1.4.0
import datetime
from run import db, app
from flask_cors import CORS
from functools import wraps
#from itertools import groupby
from flask import request, json, make_response, jsonify
from werkzeug.security import generate_password_hash, check_password_hash
from db_create import Usuarios, Bolsos, Tipos, Positions, Alertas, Productos

## swagger specific ###
from flask_swagger import swagger
from flask_swagger_ui import get_swaggerui_blueprint

SWAGGER_URL = '/api/swagger/'
API_URL = '/api/swagger_json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={'app_name': "Miura Smart"})
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)
### end swagger specific ###

#################################################################### 
cors = CORS(app, resources={r"/api/*": {"origins": "*"}}, supports_credentials=True)       ####
now = datetime.datetime.now()                                   ####
####################################################################

############ FILE specific ###############
import os
import logging
import base64 as b64
from http import HTTPStatus
from werkzeug.utils import secure_filename
from flask import Flask, flash, send_from_directory, abort

FS_LOCATION_PATH = os.getenv('FS_LOCATION_PATH', '.')
FS_DNS_URL = os.getenv('FS_DNS_URL', 'http:///18.188.179.210:9008') #cambiar por ip + puerto donde corre el backend
FILE_URL = '/api/filestorage/'
ACTUAL_PATH = os.path.dirname(__file__) + '/images/'
FILE_IMAGE = '/images'
app.config['UPLOAD_FOLDER'] = ACTUAL_PATH
# app.config['UPLOAD_FOLDER'] = FS_LOCATION_PATH

# config = {
#   'ORIGINS': [
#     'http://localhost:4200',  # React
#     'http://127.0.0.1:4200',  # React
#   ],
#   'SECRET_KEY': '...'
# }

# # App
# # app = Flask('Test')
# CORS(app, resources={ r'/*': {'origins': config['ORIGINS']}}, supports_credentials=True)

# try:
#     f = open('oauth-public.key', 'r')
#     key: str = f.read()
#     f.close()
#     app.config['SECRET_KEY'] = key
# except Exception as e:
#     app.logger.error(f'Can\'t read public key f{e}')
#     exit(-1)


############ Autenticación JWT ###########

app.config['SECRET_KEY'] = 'totemusm'
llave = app.config['SECRET_KEY']
llave2 = 'totem'
algoritmo ='HS256'

def app_context():
    with app.app_context():
        yield
app_context()

def requiere_token(f):
    @wraps(f)
    def mensajes(*args, **kwargs):
        token = request.headers.get('authorization', None)
        if not token:
            return make_response('No existe Token', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})
        try:
            token=token.split(' ')
            if len(token)>1:
              token=token[1]
            else:
              token=token[0]
            data = jwt.decode(token, llave, algoritmo)
            data = data
            #print(data)
        except:
            return make_response('Debe resgistrar su usuario', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})
        return f(*args, **kwargs)
    return mensajes


@app.route('/api/admin/login',methods=['POST'])
def login():
    auth = request.authorization
    _user_name = auth.username
    _password = auth.password
    try:
        usuario = Usuarios.query.filter_by(user_name=str(_user_name)).first()
        if check_password_hash(usuario.password,_password):
            token = {'user': _user_name,'user_id': usuario.id,'exp' : datetime.datetime.utcnow() + datetime.timedelta(hours=10)}
            token = jwt.encode(token, llave, algoritmo)
            return jsonify({'Token':token.decode('utf-8'),'User_Name':_user_name,'Nombre_Usuario':usuario.nombre,'Id_Usuario':usuario.id}) 
    except:
        return make_response('No se puede verificar el usuario', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})


@app.route('/api/swagger_json',methods=['GET'])
def spec():
    return jsonify(swagger(app))


@app.route('/api/admin/login/usuario',methods=['GET']) #### DATOS USUARIO ###
@requiere_token
def datos_usuario():
    """
        Consultar datos del usuario
        ---
        parameters:
          - in: path
            name: user_id
            type: integer
            description: ID del Usuario
        responses:
          200:
            description: Las bolsos correspondientes son:
    """
   
    token = request.headers.get('authorization', None)
    if not token:
        return make_response('No existe Token', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})
    try:
        token=token.split(' ')
        if len(token)>1:
          token=token[1]
        else:
          token=token[0]
        data = jwt.decode(token, llave, algoritmo)
    except:
        return make_response('Debe resgistrar su usuario', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})
    
    usuario =  Usuarios.query.filter_by(id=data['user_id']).first()
    db.session.commit()
    if usuario is None:
      return json.dumps([{'message': 'No existe el Usuario'}])
    else:
      return json.dumps({'User_id':usuario.id,'User_Name':usuario.user_name,'Nombre_Usuario':usuario.nombre,
                         'Perfil':usuario.perfil,'Last_login':usuario.last_login,'Date_Birth':str(usuario.date_birth),
                         "Apellido":usuario.apellido,"Email":usuario.email,"Direccion":usuario.direccion,"Foto":usuario.foto})


@app.route('/api/checkToken',methods=['POST']) 
@requiere_token
def checkToken():
    """
        Revisar si el token existe
        ---
        parameters:
        responses:
          200:
            description: Token validado exitosamente
          401:
            description: El token no es válido
    """
    return json.dumps({'mensaje' : 'Usuario validado exitosamente'})

########################################################### CREAR USUARIO ADMIN
def crear_admin():
    try:
        with app.app_context():
            admin = Usuarios.query.filter_by(user_name='ADMIN').first()
            hora_actual = str(datetime.datetime.utcnow())
            if not admin:
                _password = generate_password_hash('ac3e')
                usuario = Usuarios('ac3e', 'USM', 'Viña del Mar', 'ac3e@usm.cl', 'ADMIN', str(_password), 'ADMIN', hora_actual, hora_actual, None, None)
                db.session.add(usuario)
                db.session.commit()
            carteras = Tipos.query.filter_by(tipo_producto='Cartera').first()
            if not carteras:
                tipo = Tipos('Cartera', 60, hora_actual, None)
                db.session.add(tipo)
                db.session.commit()
            bolso = Bolsos.query.all()
            if len(bolso)==0:
                #cartera = Bolsos('Cartera', 'Cartera_AC3E', None, hora_actual, None, None, None)
                cartera = Bolsos('Cartera', 'Alaska', 'Cartera AC3E', 'Cartera_AC3E', None, hora_actual, None, '1', hora_actual, 'true', '1F:74:AB:DF:E4:93')
                db.session.add(cartera)
                db.session.commit()
    except Exception as e:
        logging.warning(e)

crear_admin()
########################################################### Gestión de Usuarios

@app.route('/api/admin/login/usuario/token',methods=['GET']) #### TRAER USUARIO POR TOKEN ###
@requiere_token
def datos_usuario_token():
    """
        Consultar datos del usuario
    """
    token = request.headers.get('authorization', None)
    if not token:
        return make_response('No existe Token', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})
    try:
        token=token.split(' ')
        if len(token)>1:
          token=token[1]
        else:
          token=token[0]
        data = jwt.decode(token, llave, algoritmo)
    except:
        return make_response('Debe resgistrar su usuario', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})
    
    usuario =  Usuarios.query.filter_by(id=data['user_id']).first()
    db.session.commit()
    return str(usuario)


@app.route('/api/admin/users/cargar',methods=['GET']) #### TRAER USUARIOS #####
@requiere_token
def traer_usuario():
    """
        Traer Usuarios
        ---
        responses:
          200:
            description: Los Usuarios se cargaron correctamente
    """ 
    usuarios = Usuarios.query.all()
    usuarios_json = []
    for row in usuarios:
        if row.deleted_on == None:
          usuarios_json.append(json.loads(str(row)))
        else:
          continue
    return json.dumps(usuarios_json)


@app.route('/api/admin/users/agregar',methods=['POST']) ### AGREGAR USUARIO ###
@requiere_token
def crear_usuario():
    """
        Agregar Usuario
        ---
        parameters:
          - in: formData
            name: nombre
            type: string
            description: Nombre del Usuario
          - in: formData
            name: apellido
            type: string
            description: Apellido del Usuario
          - in: formData
            name: direccion
            type: string
            description: Direccion del Usuario
          - in: formData
            name: email
            type: string
            description: Email del Usuario
          - in: formData
            name: user_name
            type: string
            description: Nombre de Usuario
          - in: formData
            name: password
            type: string
            description: Contraseña
          - in: formData
            name: perfil
            type: string
            description: Perfil de Usuario
          - in: formData
            name: date_birth
            type: string
            description: Fecha de Nacimiento
        responses:
          200:
            description: Usuario agregado correctamente en la base de datos
    """
    try:
        _nombre = request.form['nombre']
        _apellido = request.form['apellido']
        _direccion = request.form['direccion']
        _email = request.form['email']
        _user_name = request.form['user_name']
        _password = request.form['password']
        _perfil =request.form['perfil']
        _date_birth =request.form['date_birth']
        if _user_name and _password:
            _password = generate_password_hash(_password)
            add_res = Usuarios(str(_nombre),str(_apellido),str(_direccion),str(_email),str(_user_name),str(_password),str(_perfil),None,str(_date_birth),None,None)
            db.session.add(add_res)
            db.session.commit()
            return json.dumps({'mensaje' : 'Usuario ingresado correctamente'})
        else:
            return json.dumps({'mensaje' : 'Ingrese los campos requeridos'})
    except Exception as e:
        return json.dumps({'error...':str(e)})
    
    
@app.route('/api/admin/users/editar',methods=['PUT']) #### EDITAR USUARIO #####
@requiere_token
def editar_usuario():
    """
        Editar Usuario
        ---
        parameters:
          - in: formData
            name: user_id
            type: integer
            description: ID del Usuario
          - in: formData
            name: nombre
            type: string
            description: Nombre del Usuario
          - in: formData
            name: apellido
            type: string
            description: Apellido del Usuario
          - in: formData
            name: direccion
            type: string
            description: Direccion del Usuario
          - in: formData
            name: email
            type: string
            description: Email del Usuario
          - in: formData
            name: user_name
            type: string
            description: Nombre de Usuario
          - in: formData
            name: password
            type: string
            description: Contraseña
          - in: formData
            name: perfil
            type: string
            description: Perfil de Usuario
          - in: formData
            name: date_birth
            type: string
            description: Fecha de Nacimiento
          - in: formData
            name: deleted_on
            type: string
            description: Fecha de Eliminación
        responses:
          200:
            description: Usuario editado correctamente en la base de datos
    """
    _user_id = request.form['user_id']
    _nombre = request.form['nombre']
    _apellido = request.form['apellido']
    _direccion = request.form['direccion']
    _email = request.form['email']
    _user_name = request.form['user_name']
    # _password = request.form['password']
    _perfil =request.form['perfil']
    _date_birth =request.form['date_birth']
    usuario = Usuarios.query.filter_by(id=_user_id).first()
    usuario.nombre = _nombre
    usuario.apellido = _apellido
    usuario.direccion = _direccion
    usuario.email = _email
    # usuario.user_name = _user_name
    # _password = generate_password_hash(_password)
    # usuario.password = _password
    # usuario.perfil = _perfil
    usuario.date_birth = _date_birth
    usuario.deleted_on = None
    if usuario.user_name == 'ADMIN':
        usuario.user_name = 'ADMIN'
        usuario.perfil = 'ADMIN'
        db.session.commit()
        return json.dumps({'message':'No se puede editar el Nombre de Usuario o Perfil de este Usuario'})
    else:
        usuario.user_name = _user_name
        usuario.perfil = _perfil
        db.session.commit()
        return json.dumps({'message':'El usuario se ha editado correctamente en la base de datos'})

@app.route('/api/admin/users/eliminar',methods=['PUT']) ##### ELIMINAR USUARIO #####
@requiere_token
def eliminar_usuario():
    """
        Eliminar Usuario
        ---
        parameters:
          - in: formData
            name: usuario_id
            type: integer
            description: ID del Usuario
        responses:
          200:
            description: Usuario editado correctamente en la base de datos
    """
    _user_id = request.form['user_id']
    _deleted_on = str(datetime.datetime.utcnow())

    usuario = Usuarios.query.filter_by(id=_user_id).first()
    usuario.deleted_on = _deleted_on
    db.session.commit()
    return json.dumps({'message':'El Usuario se ha eliminado correctamente de la base de datos'})

    
############################################################# Gestión de Bolsos

@app.route('/api/admin/bolsos/cargartodos',methods=['GET']) #### TRAER BOLSOS ######
@requiere_token
def traer_bolsos():
    """
        Traer Bolsos
        ---
        responses:
          200:
            description: Los Bolsos se cargaron correctamente
    """ 
    bolsos = Bolsos.query.all()
    bolsos_json = []
    for row in bolsos:
        if row.deleted_on == None:
          bolsos_json.append(json.loads(str(row)))
        else:
          continue
    db.session.commit()
    return json.dumps(bolsos_json)


@app.route('/api/admin/bolsos/cargarbyid',methods=['GET']) #### TRAER BOLSOS POR ID ######
@requiere_token
def traer_bolso_id():
    """
        Traer Bolsos
        ---
        responses:
          200:
            description: Los Bolsos se cargaron correctamente
    """ 
    _bolso_id = request.form['bag_id']
    bolsos = Bolsos.query.filter_by(id=_bolso_id).first()
    if  bolsos is None:
      return json.dumps([{'message': 'No hay bolsos cargados para este usuario'}])
    db.session.commit()
    return str(bolsos)


@app.route('/api/admin/bolsos/byuserform',methods=['GET']) #### BOLSOS POR USUARIO ######
@requiere_token
def bolsos_by_user_form():
    """
        Traer Bolsos por Usuario
        ---
        parameters:
          - in: formData
            name: userid
            type: integer
            description: Id del Usuario
        responses:
          200:
            description: Los Bolsos se cargaron correctamente
    """ 
    _userid = request.form['userid']

    bolsos = Bolsos.query.filter_by(user_id=_userid)
    if  bolsos is None:
      return json.dumps([{'message': 'No hay bolsos cargados para este usuario'}])
    bolsos_json = []
    for row in bolsos:
        if row.deleted_on == None:
          bolsos_json.append(json.loads(str(row)))
        else:
          continue
    db.session.commit()
    return json.dumps(bolsos_json)


@app.route('/api/admin/bolsos/byuser',methods=['GET']) #### BOLSOS POR USUARIO TOKEN ######
@requiere_token
def bolsos_by_user():
    """
        Traer Bolsos por Usuario
        ---
        parameters:
          - in: formData
            name: userid
            type: integer
            description: Id del Usuario
        responses:
          200:
            description: Los Bolsos se cargaron correctamente
    """ 
    token = request.headers.get('authorization', None)
    if not token:
        return make_response('No existe Token', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})
    try:
        token=token.split(' ')
        if len(token)>1:
          token=token[1]
        else:
          token=token[0]
        data = jwt.decode(token, llave, algoritmo)
        data = data
    except:
        return make_response('Debe resgistrar su usuario', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})

    bolsos = Bolsos.query.filter_by(user_id=data['user_id'])
    if  bolsos is None:
      return json.dumps([{'message': 'No hay bolsos cargados para este usuario'}])
    bolsos_json = []
    for row in bolsos:
        if row.deleted_on == None:
          producto = Productos.query.filter_by(modelo=row.modelo).first()
          if  producto is None:
            imagen = row.imagen
          else:
            imagen = producto.imagen
          # db.session.commit()
          bolso = json.dumps({'id':row.id,'tipo':row.tipo,'modelo':row.modelo,'namebyuser':row.namebyuser,
                          'product_name':row.product_name,'date_create':str(row.date_create),'deleted_on':str(row.deleted_on),
                          "imagen":imagen,"user_id":row.user_id,"register_date":str(row.register_date),"activo":row.activo,"mac_address":row.mac_address})
          bolsos_json.append(json.loads(str(bolso)))
        else:
          continue
    db.session.commit()
    return json.dumps(bolsos_json)

    # usuario =  Usuarios.query.filter_by(id=data['user_id']).first()
    # db.session.commit()
    # if usuario is None:
    #   return json.dumps([{'message': 'No existe el Usuario'}])
    # else:
    #   return json.dumps({'User_id':usuario.id,'User_Name':usuario.user_name,'Nombre_Usuario':usuario.nombre,
    #                      'Perfil':usuario.perfil,'Last_login':usuario.last_login,'Date_Birth':str(usuario.date_birth),
    #                      "Apellido":usuario.apellido,"Email":usuario.email,"Direccion":usuario.direccion,"Foto":usuario.foto})

@app.route('/api/admin/bolsos/agregar',methods=['POST']) ### AGREGAR BOLSO ####
@requiere_token
def crear_bolso():
    """
        Agregar Bolsos
        ---
        parameters:
          - in: formData
            name: tipo
            type: string
            description: Tipo de Producto
          - in: formData
            name: product_name
            type: string
            description: Nombre de Producto
          - in: formData
            name: imagen
            type: string
            description: URL Imagen
          - in: formData
            name: date_create
            type: string
            description: Fecha de Creación
        responses:
          200:
            description: Bolso agregado correctamente en la base de datos
    """
    try:
      hora_actual = str(datetime.datetime.utcnow())
      _tipo = request.form['tipo']
      _modelo = request.form['modelo']
      _product_name = request.form['product_name']
      _user_id = request.form['user_id'] if request.form['user_id'] != '' else  None
      _register_date = hora_actual if request.form['user_id'] != '' else  None
      _namebyuser = request.form['namebyuser'] if request.form['namebyuser'] != '' else  None
      _activo = request.form['activo']
      _date_create = hora_actual
      if _tipo and _product_name:
          add_res = Bolsos(str(_tipo),str(_modelo), str(_namebyuser),str(_product_name),None,_date_create,None,_user_id,_register_date,str(_activo),None)
          db.session.add(add_res)
          db.session.commit()
          return json.dumps({'mensaje' : 'Bolso ingresado correctamente'})
      else:
          return json.dumps({'mensaje' : 'Ingrese los campos requeridos'})
    except Exception as e:
        return json.dumps({'error...':str(e)})
    
    
@app.route('/api/admin/bolsos/editar',methods=['PUT']) ##### EDITAR BOLSO #####
@requiere_token
def editar_bolso():
    """
        Editar Bolsos
        ---
        parameters:
          - in: formData
            name: bolso_id
            type: integer
            description: ID del Bolso
          - in: formData
            name: tipo
            type: string
            required: false
            description: Tipo de Producto
          - in: formData
            name: product_name
            type: string
            required: false
            description: Nombre de Producto
          - in: formData
            name: imagen
            type: string
            required: false
            description: URL Imagen
          - in: formData
            name: date_create
            type: string
            required: false
            description: Fecha de Creación
          - in: formData
            name: deleted_on
            type: string
            required: false
            description: Fecha de Eliminación
          - in: formData
            name: user_id
            type: integer
            required: false
            description: ID del Usuario
          - in: formData
            name: register_date
            type: string
            required: false
            description: Fecha de Registro a un Usuario
            - in: formData
            name: activo
            type: string
            required: false
            description: Indicador de estado Activo
        responses:
          200:
            description: Bolso editado correctamente en la base de datos
    """
    _bolso_id = request.form['bolso_id']
    _tipo = request.form['tipo']
    _product_name = request.form['product_name']
    _imagen =request.form['imagen']
    _date_create = request.form['date_create']
    _deleted_on =request.form['deleted_on']
    _user_id =request.form['user_id']
    _register_date =request.form['register_date']
    _activo =request.form['activo']
    bolso = Bolsos.query.filter_by(id=_bolso_id).first()
    if _deleted_on == '':
      _deleted_on = None
    bolso.tipo = _tipo if _tipo != '' else bolso.tipo
    bolso.product_name = _product_name if _product_name != '' else bolso.product_name
    bolso.imagen = _imagen if _imagen != '' else bolso.imagen
    bolso.date_create = _date_create if _date_create != '' else bolso.date_create
    bolso.deleted_on = _deleted_on if _deleted_on != '' else bolso.deleted_on
    bolso.user_id = _user_id if _user_id  != '' else bolso.user_id 
    bolso.register_date = _register_date if _register_date != '' else bolso.register_date
    bolso.activo = _activo if _activo != '' else bolso.activo
    db.session.commit()
    return json.dumps({'message':'El bolso se ha editado correctamente en la base de datos'})


@app.route('/api/admin/bolsos/editarbolso',methods=['PUT']) ##### EDITAR BOLSO #####
@requiere_token
def editar_bolso_():
    """
        Editar Bolsos
        ---
        parameters:
          - in: formData
            name: bolso_id
            type: integer
            description: ID del Bolso
          - in: formData
            name: tipo
            type: string
            required: false
            description: Tipo de Producto
          - in: formData
            name: product_name
            type: string
            required: false
            description: Nombre de Producto
            - in: formData
            name: activo
            type: string
            required: false
            description: Indicador de estado Activo
        responses:
          200:
            description: Bolso editado correctamente en la base de datos
    """
    _bolso_id = request.form['bolso_id']
    _tipo = request.form['tipo']
    _modelo = request.form['modelo']
    _user_id = request.form['user_id']
    if _user_id == 'undefined':
      _user_id = None
    _namebyuser = request.form['namebyuser']
    _product_name = request.form['product_name']
    _activo = request.form['activo']

    bolso = Bolsos.query.filter_by(id=_bolso_id).first()
    bolso.tipo = _tipo if _tipo != '' else bolso.tipo
    bolso.user_id = _user_id if _user_id != '' else bolso.user_id
    bolso.modelo = _modelo if _modelo != '' else bolso.modelo
    bolso.namebyuser = _namebyuser if _namebyuser != '' else bolso.namebyuser
    bolso.product_name = _product_name if _product_name != '' else bolso.product_name
    bolso.activo = _activo if _activo != '' else bolso.activo
    db.session.commit()
    return json.dumps({'message':'El bolso se ha editado correctamente en la base de datos'})


@app.route('/api/admin/bolsos/eliminarbolso',methods=['PUT']) ##### ELIMINAR BOLSO #####
@requiere_token
def eliminar_bolso_():
    """
        Eliminar Bolsos
        ---
        parameters:
          - in: formData
            name: bolso_id
            type: integer
            description: ID del Bolso
        responses:
          200:
            description: Bolso eliminado correctamente en la base de datos
    """
    _bolso_id = request.form['bolso_id']
    _user_id = None
    _deleted_on = str(datetime.datetime.utcnow())

    bolso = Bolsos.query.filter_by(id=_bolso_id).first()
    bolso.user_id= _user_id
    bolso.deleted_on = _deleted_on
    db.session.commit()
    return json.dumps({'message':'El bolso se ha eliminado correctamente en la base de datos'})


@app.route('/api/admin/bolsos/asignar',methods=['POST']) ### ASIGNAR BOLSO ####
@requiere_token
def asignar_bolso():
    """
        Asignar Bolsos
        ---
        parameters:
          - in: formData
            name: register_date
            type: string
            description: Fecha de Asignación
          - in: formData
            name: bolso_id'
            type: integer
            description: Id del Bolso
          - in: formData
            name: user_id
            type: integer
            description: Id del Ussuario
        responses:
          200:
            description: Bolso asignado correctamente al usuario
    """
    try:
        _fecha = request.form['register_date']
        _bolso_id = request.form['bolso_id']
        _user_id =request.form['user_id']
        if _bolso_id and _user_id:
            bolso = Bolsos.query.filter_by(id=_bolso_id).first()
            bolso.user_id = _user_id
            bolso.register_date = _fecha
            db.session.commit()
            return json.dumps({'mensaje' : 'Bolso asignado correctamente'})
        else:
            return json.dumps({'mensaje' : 'Ingrese los campos requeridos'})
    except Exception as e:
        return json.dumps({'error...':str(e)})


@app.route('/api/admin/bolsos/cargar_tipos',methods=['GET']) ## TRAER TIPOS ###
@requiere_token
def traer_tipos():
    """
        Traer Tipos de Bolsos
        ---
        responses:
          200:
            description: Los Tipos se cargaron correctamente
    """ 
    tipos = Tipos.query.all()
    tipos_json = []
    for row in tipos:
        tipos_json.append(json.loads(str(row)))
    return json.dumps(tipos_json)


@app.route('/api/admin/bolsos/cargar_productos',methods=['GET']) ## TRAER PRODUCTOS ###
@requiere_token
def traer_productos():
    """
        Traer Tipos de Productos
        ---
        responses:
          200:
            description: Los productos se cargaron correctamente
    """ 
    productos = Productos.query.all()
    if  productos is None:
      return json.dumps([{'message': 'No hay Productas cargados en la Base de Datos'}])
    productos_json = []
    for row in productos:
        if row.deleted_on == None:
          productos_json.append(json.loads(str(row)))
        else:
          continue
    db.session.commit()
    return json.dumps(productos_json)

    
@app.route('/api/admin/bolsos/agregar_producto',methods=['POST']) # AGREGAR PRODUCTO ##
@requiere_token
def crear_producto():
    """
        Agregar Producto
        ---
        parameters:
          - in: formData
            name: tipo
            type: string
            description: Tipo de Producto
          - in: formData
            name: modelo
            type: string
            description: Modelo de Producto
        responses:
          200:
            description: Producto agregado correctamente en la base de datos
    """
    try:
        _tipo = request.form['tipo']
        _modelo = request.form['modelo']
        if _tipo:
            add_res = Productos(str(_tipo),str(_modelo),None,None)
            db.session.add(add_res)
            db.session.commit()
            return json.dumps({'mensaje' : 'Producto ingresado correctamente'})
        else:
            return json.dumps({'mensaje' : 'Ingrese los campos requeridos'})
    except Exception as e:
        return json.dumps({'error...':str(e)})
    
    
@app.route('/api/admin/bolsos/editar_producto',methods=['PUT']) ### EDITAR PRODUCTO ###
@requiere_token
def editar_producto():
    """
        Editar Producto
        ---
        parameters:
          - in: formData
            name: producto_id
            type: integer
            description: Producto ID
          - in: formData
            name: tipo
            type: string
            description: Tipo de Producto
          - in: formData
            name: modelo
            type: string
            description: Modelo de Producto
        responses:
          200:
            description: Producto editado correctamente en la base de datos
    """
    _producto_id = request.form['producto_id']
    _tipo = request.form['tipo']
    _modelo = request.form['modelo']
    
    producto = Productos.query.filter_by(id=_producto_id).first()
    producto.tipo = _tipo
    producto.modelo = _modelo
    db.session.commit()
    return json.dumps({'message':'El Producto se ha editado correctamente en la base de datos'})


@app.route('/api/admin/bolsos/eliminar_producto',methods=['PUT']) ### ELIMINAR PRODUCTO ###
@requiere_token
def eliminar_producto():
    """
        Eliminar Producto
        ---
        parameters:
          - in: formData
            name: producto_id
            type: integer
            description: Producto ID
        responses:
          200:
            description: Producto eliminado correctamente en la base de datos
    """
    _producto_id = request.form['producto_id']
    _deleted_on = str(datetime.datetime.utcnow())
    
    producto = Productos.query.filter_by(id=_producto_id).first()
    producto.deleted_on = _deleted_on
    db.session.commit()
    return json.dumps({'message':'El Producto se ha eliminado correctamente en la base de datos'})


@app.route('/api/admin/bolsos/editar_producto_img/<product_id>',methods=['POST']) ### EDITAR PRODUCTO IMG ###
@requiere_token
def editar_producto_img(product_id):
    """
        Editar Imagen del Producto
        ---
        parameters:
          - in: formData
            name: tipo_id
            type: integer
            description: Tipo ID
          - in: formData
            name: imagen
            type: string
            description: Imagen del Producto
        responses:
          200:
            description: Producto editado correctamente en la base de datos
    """
    _producto_id = product_id

    if 'file' not in request.files:
        abort(HTTPStatus.BAD_REQUEST, "No Multipart file found")
    file = request.files['file']

    if file.filename == '':
        flash('No selected File')
        abort(HTTPStatus.NOT_FOUND, "No selected file")
    encoding = "utf-8"
    filename: str = secure_filename(file.filename)
    filename_ext: str = filename + str(datetime.datetime.timestamp(datetime.datetime.utcnow()))
    filename_coded: str = b64.b64encode(filename_ext.encode(encoding)).decode(encoding)
    ext = ""
    if len(filename.split('.')) > 1:
        ext = "." + filename.split('.')[-1]
    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename_coded + ext))
    # file.save(os.path.join(FS_DNS_URL + ACTUAL_PATH, filename_coded + ext))
    dict_ret = {'url': FS_DNS_URL + FILE_URL + filename_coded + ext }
    # dict_ret = {'url': FS_DNS_URL + ACTUAL_PATH + filename_coded + ext }
    file.close()

    producto = Productos.query.filter_by(id=_producto_id).first()
    producto.imagen = FS_DNS_URL + FILE_URL + filename_coded + ext
    # producto.imagen = FS_DNS_URL + ACTUAL_PATH + FILE_URL + filename_coded + ext
    db.session.commit()

    return jsonify(dict_ret), HTTPStatus.CREATED


@app.route('/api/admin/bolsos/agregar_tipo',methods=['POST']) # AGREGAR TIPO ##
@requiere_token
def crear_tipo():
    """
        Agregar Tipo de Bolsos
        ---
        parameters:
          - in: formData
            name: tipo
            type: string
            description: Tipo de Producto
          - in: formData
            name: position_freq
            type: string
            description: Frecuencia de envío de Posición
          - in: formData
            name: date_create
            type: string
            description: Fecha de Creación
        responses:
          200:
            description: Tipo de Producto agregado correctamente en la base de datos
    """
    try:
        _tipo = request.form['tipo']
        _position_freq = request.form['position_freq']
        _date_create =request.form['date_create']
        if _tipo:
            add_res = Tipos(str(_tipo),str(_position_freq),str(_date_create),None)
            db.session.add(add_res)
            db.session.commit()
            return json.dumps({'mensaje' : 'Tipo de Producto ingresado correctamente'})
        else:
            return json.dumps({'mensaje' : 'Ingrese los campos requeridos'})
    except Exception as e:
        return json.dumps({'error...':str(e)})
    
    
@app.route('/api/admin/bolsos/editar_tipo',methods=['PUT']) ### EDITAR TIPO ###
@requiere_token
def editar_tipo():
    """
        Editar Tipo de Bolso
        ---
        parameters:
          - in: formData
            name: tipo_id
            type: integer
            description: Tipo ID
          - in: formData
            name: tipo
            type: string
            description: Tipo de Producto
          - in: formData
            name: position_freq
            type: string
            description: Frecuencia de envío de Posición
          - in: formData
            name: date_create
            type: string
            description: Fecha de Creación
          - in: formData
            name: deleted_on
            type: string
            description: Fecha de Eliminación
        responses:
          200:
            description: Tipo de Bolso editado correctamente en la base de datos
    """
    _tipo_id = request.form['tipo_id']
    _tipo = request.form['tipo']
    _position_freq = request.form['position_freq']
    _date_create =request.form['date_create']
    _deleted_on =request.form['deleted_on']
    
    tipo = Tipos.query.filter_by(id=_tipo_id).first()
    tipo.tipo_producto = _tipo
    tipo.position_freq = _position_freq
    tipo.date_create = _date_create
    tipo.deleted_on = _deleted_on
    db.session.commit()
    return json.dumps({'message':'El tipo se ha editado correctamente en la base de datos'})


############################################### Gestión de Búzquedas y Reportes

@app.route('/api/admin/bolsos/<bolso_id>',methods=['GET']) ## OBTENER BOLSOS ##
@requiere_token
def consulta_bolso(bolso_id):
    """
        Consultar bolsos por usuario
        ---
        parameters:
          - in: path
            name: user_id
            type: integer
            description: ID del Usuario
        responses:
          200:
            description: Las bolsos correspondientes son:
    """
    bolsos = Bolsos.query.filter_by(id=bolso_id).first()

    if  bolsos is None:
      return json.dumps([{'message': 'No hay bolsos cargados para este usuario'}])

    if bolsos.deleted_on == None:
      producto = Productos.query.filter_by(modelo=bolsos.modelo).first()
      if  producto is None:
        imagen = bolsos.imagen
      else:
        imagen = producto.imagen

      bolso = json.dumps({'id':bolsos.id,'tipo':bolsos.tipo,'modelo':bolsos.modelo,'namebyuser':bolsos.namebyuser,
                      'product_name':bolsos.product_name,'date_create':str(bolsos.date_create),'deleted_on':str(bolsos.deleted_on),
                      "imagen":imagen,"user_id":bolsos.user_id,"register_date":str(bolsos.register_date),"activo":bolsos.activo,"mac_address":bolsos.mac_address})
    else:
      return json.dumps([{'message': 'Este bolso ha sido eliminado'}])
    db.session.commit()
    return str(bolso)

@app.route('/api/admin/bolsos/usuario',methods=['GET']) #### OBTENER BOLSOS ###
@requiere_token
def consulta_bolsos():
    """
        Consultar bolsos por usuario
        ---
        parameters:
          - in: path
            name: user_id
            type: integer
            description: ID del Usuario
        responses:
          200:
            description: Las bolsos correspondientes son:
    """
   
    token = request.headers.get('authorization', None)
    if not token:
        return make_response('No existe Token', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})
    try:
        token=token.split(' ')
        if len(token)>1:
          token=token[1]
        else:
          token=token[0]
        data = jwt.decode(token, llave, algoritmo)
        data = data
    except:
        return make_response('Debe resgistrar su usuario', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})

    bolsos_return = []
    bolsos = Bolsos.query.filter_by(user_id=data['user_id']).all()
    if bolsos is None:
      return json.dumps([{'message': 'No hay bolsos cargados'}])
    for bolso in bolsos:
      bolsos_return.append(json.loads(str(bolso)))
    db.session.commit()
    return json.dumps(bolsos_return)


@app.route('/api/admin/bolsos/reportar',methods=['POST']) ## REPORTAR BOLSO ###
@requiere_token
def reportar_bolso():
    """
        Reporte de Ubicación de Bolso
        ---
        parameters:
          - in: formData
            name: time_stamp
            type: string
            description: Hora del Reporte
          - in: formData
            name: bolso_id
            type: integer
            description: ID del bolso
          - in: formData
            name: user_id
            type: integer
            description: ID del Usuario
          - in: formData
            name: latitud
            type: string
            description: Latitud
          - in: formData
            name: longitud
            type: string
            description: Longitud
        responses:
          200:
            description: Reporte de ubicación ingresado correctamente
    """
    try:
        _time_stamp = request.form['time_stamp']
        _bolso_id = request.form['bolso_id']
        _user_id = request.form['user_id']
        _latitud = request.form['latitud']
        _longitud = request.form['longitud']
        if _latitud and _longitud:
            add_res = Positions(str(_time_stamp),str(_bolso_id),str(_user_id),str(_latitud),str(_longitud))
            db.session.add(add_res)
            db.session.commit()
            return json.dumps({'mensaje' : 'Posición registrada correctamente'})
        else:
            return json.dumps({'mensaje' : 'Ingrese los campos requeridos'})
    except Exception as e:
        return json.dumps({'error...':str(e)})


@app.route('/api/admin/bolsos/alertar',methods=['POST']) #### ALERTAR BOLSO ###
@requiere_token
def alertar_bolso():
    """
        Alerta de Bolso
        ---
        parameters:
          - in: formData
            name: time_stamp
            type: string
            description: Hora del Reporte
          - in: formData
            name: bolso_id
            type: integer
            description: ID del bolso
          - in: formData
            name: user_id
            type: integer
            description: ID del Usuario
          - in: formData
            name: alert_name
            type: string
            description: Nombre de Alerta
          - in: formData
            name: status
            type: string
            description: Status
          - in: formData
            name: latitud
            type: string
            description: Latitud
          - in: formData
            name: longitud
            type: string
            description: Longitud
        responses:
          200:
            description: Alerta ingresada correctamente
    """
    try:
        _time_stamp = str(datetime.datetime.utcnow())
        _bolso_id = request.form['bolso_id']
        _user_id = request.form['user_id']
        _alert_name = request.form['alert_name']
        _status = request.form['status']
        _latitud = request.form['latitud']
        _longitud = request.form['longitud']
        if _latitud and _longitud:
            add_res = Alertas(str(_time_stamp),str(_bolso_id),str(_user_id),str(_alert_name),str(_status),str(_latitud),str(_longitud))
            db.session.add(add_res)
            db.session.commit()
            return json.dumps({'mensaje' : 'Posición registrada correctamente'})
        else:
            return json.dumps({'mensaje' : 'Ingrese los campos requeridos'})
    except Exception as e:
        return json.dumps({'error...':str(e)})
    

@app.route('/api/admin/bolsos/alerta/editar',methods=['PUT']) #### EDITAR ALERTA BOLSO ###
@requiere_token
def alerta_bolso_editar():
    """
        Editar Alerta de Bolso
        ---
        parameters:
          - in: formData
            name: alerta_id
            type: integer
            description: ID del bolso
          - in: formData
            name: status
            type: string
            description: Status
        responses:
          200:
            description: Alerta actualizada correctamente
    """
    try:
      _time_stamp = str(datetime.datetime.utcnow())
      _alerta_id = request.form['alerta_id']
      _status = request.form['status']

      alerta = Alertas.query.filter_by(id=_alerta_id).first()
      alerta.time_stamp = _time_stamp
      alerta.status = _status
      db.session.commit()
      #return str(alerta)
      return json.dumps({'mensaje' : 'Alerta actualizada correctamente'})
    except Exception as e:
        return json.dumps({'error...':str(e)})
    
@app.route('/api/admin/bolsos/alertas/userform',methods=['GET']) ## ALERTAS POR USUARIO ##
@requiere_token
def alertas_usuario_form():
    """
        Consultar alertas por usuario
        ---
        parameters:
          - in: path
            name: user_id
            type: integer
            description: ID del Usuario
        responses:
          200:
            description: Las alertas asociadas son:
    """
    try:
      _user_id = request.form['user_id']
      alertas = Alertas.query.filter_by(user_id=_user_id)
      alertas_json = []
      for row in alertas:
          alertas_json.append(json.loads(str(row)))
      db.session.commit()
      return json.dumps(alertas_json)
    except Exception as e:
      return json.dumps({'error...':str(e)})


@app.route('/api/admin/bolsos/alertas/userformput',methods=['PUT']) ## ALERTAS POR USUARIO ##
@requiere_token
def alertas_usuario_formput():
    """
        Consultar alertas por usuario
        ---
        parameters:
          - in: path
            name: user_id
            type: integer
            description: ID del Usuario
        responses:
          200:
            description: Las alertas asociadas son:
    """
    try:
      _user_id = request.form['user_id']
      alertas = Alertas.query.filter_by(user_id=_user_id).order_by(Alertas.id.desc()).limit(50)
      alertas_json = []
      for row in alertas:
          alertas_json.append(json.loads(str(row)))
      db.session.commit()
      return json.dumps(alertas_json)
    except Exception as e:
      return json.dumps({'error...':str(e)})

@app.route('/api/admin/bolsos/alertas/user',methods=['GET']) ## ALERTAS POR USUARIO ##
@requiere_token
def alertas_usuario():
    """
        Consultar alertas por usuario
        ---
        parameters:
          - in: path
            name: user_id
            type: integer
            description: ID del Usuario
        responses:
          200:
            description: Las alertas asociadas son:
    """
    token = request.headers.get('authorization', None)
    if not token:
        return make_response('No existe Token', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})
    try:
        token=token.split(' ')
        if len(token)>1:
          token=token[1]
        else:
          token=token[0]
        data = jwt.decode(token, llave, algoritmo)
    except:
        return make_response('Debe resgistrar su usuario', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})

    alertas = Alertas.query.filter_by(user_id=data['user_id']).order_by(Alertas.id.desc()).limit(50)
    alertas_json = []
    for row in alertas:
      alertas_json.append(json.loads(str(row)))
    db.session.commit()
    return json.dumps(alertas_json)


@app.route('/api/admin/bolsos/alertas/userbag',methods=['GET']) ## ALERTAS POR USUARIO ##
@requiere_token
def alertas_usuariobag():
    """
        Consultar alertas por usuario
        ---
        parameters:
          - in: path
            name: user_id
            type: integer
            description: ID del Usuario
        responses:
          200:
            description: Las alertas asociadas son:
    """
    token = request.headers.get('authorization', None)
    if not token:
        return make_response('No existe Token', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})
    try:
        token=token.split(' ')
        if len(token)>1:
          token=token[1]
        else:
          token=token[0]
        data = jwt.decode(token, llave, algoritmo)
    except:
        return make_response('Debe resgistrar su usuario', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})

    alertas = Alertas.query.filter_by(user_id=data['user_id']).order_by(Alertas.id.desc()).limit(50)
    alertas_json = []
    for row in alertas:
      bolso = Bolsos.query.filter_by(id=row.bolso_id).first()
      alerta = json.dumps({'id':row.id,'time_stamp':str(row.time_stamp),'bolso_id':row.bolso_id,'user_id':row.user_id,'alert_name':row.alert_name,
                          'status':row.status,'latitud':row.latitud,'longitud':row.longitud,"bolso_name":bolso.namebyuser})
      alertas_json.append(json.loads(str(alerta)))
    db.session.commit()
    return json.dumps(alertas_json)


@app.route('/api/admin/bolsos/alertas/bag/<bolso_id>',methods=['GET']) ## ALERTAS POR BOLSO ##
@requiere_token
def alertas_bolso(bolso_id):
    """
        Consultar alertas por bolso
        ---
        parameters:
          - in: path
            name: bolso_id
            type: integer
            description: ID del Bolso
        responses:
          200:
            description: Las alertas asociadas son:
    """
    alertas = Alertas.query.filter_by(bolso_id=bolso_id).first()
    db.session.commit()
    return str(alertas)


@app.route('/api/admin/bolsos/alertas/eliminar',methods=['POST']) ## ELIMINAR ALERTA ##
@requiere_token
def alertas_usuario_eliminar():
    """
        Consultar alertas por usuario
        ---
        parameters:
          - in: path
            name: id
            type: integer
            description: ID del Usuario
        responses:
          200:
            description: Las alertas asociadas son:
    """
    try:
      _alerta_id = request.form['id']
      Alertas.query.filter_by(id=_alerta_id).delete()
      db.session.commit()
      return json.dumps({'Mensaje':'Registro eliminado correctamente'})
    except Exception as e:
      return json.dumps({'error...':str(e)})



@app.route('/api/admin/bolsos/reportes/user',methods=['GET']) ## REPORTE POR USUARIO ##
@requiere_token
def reporte_usuario():
    """
        Consultar reportes por usuario
        ---
        parameters:
          - in: path
            name: user_id
            type: integer
            description: ID del Usuario
        responses:
          200:
            description: Los reportes asociadas son:
    """
    token = request.headers.get('authorization', None)
    if not token:
        return make_response('No existe Token', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})
    try:
        token=token.split(' ')
        if len(token)>1:
          token=token[1]
        else:
          token=token[0]
        data = jwt.decode(token, llave, algoritmo)
        data = data
    except:
        return make_response('Debe resgistrar su usuario', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})

    ubicaciones = Positions.query.filter_by(user_id=data['user_id'])
    ubicaciones_json = []
    for row in ubicaciones:
        ubicaciones_json.append(json.loads(str(row)))
    db.session.commit()
    return json.dumps(ubicaciones_json)


@app.route('/api/admin/bolsos/reportes/userform',methods=['GET']) ## REPORTE POR USUARIO FORM ##
@requiere_token
def reporte_usuario_form():
    """
        Consultar reportes por usuario
        ---
        parameters:
          - in: path
            name: user_id
            type: integer
            description: ID del Usuario
        responses:
          200:
            description: Los reportes asociadas son:
    """
    _user_id = request.form['user_id']
    ubicaciones = Positions.query.filter_by(user_id=_user_id)
    ubicaciones_json = []
    for row in ubicaciones:
        ubicaciones_json.append(json.loads(str(row)))
    db.session.commit()
    return json.dumps(ubicaciones_json)


@app.route('/api/admin/bolsos/reportes/bag',methods=['PUT']) ## REPORTE POR BOLSO ##
@requiere_token
def reporte_bolso():
    """
        Consultar reportes por bolso
        ---
        parameters:
          - in: formData
            name: bag_id
            type: integer
            description: ID del Bolso
        responses:
          200:
            description: Los reportes asociadas son:
    """
    _bolso_id = request.form['bolso_id']
    ubicaciones = Positions.query.filter_by(bolso_id=_bolso_id).order_by(Positions.id.desc()).limit(20)
    ubicaciones_json = []
    for row in ubicaciones:
        ubicaciones_json.append(json.loads(str(row)))
    db.session.commit()
    #ubicaciones_json_inv = ubicaciones_json[::-1]
    return json.dumps(ubicaciones_json)


####################################################### Gestión de Archivos

# @app.route("/api/filestorage/spec", methods=['GET'])
# @requiere_token
# def spec():
#     swag = swagger(app)
#     swag['info']['version'] = "1.0"
#     swag['info']['title'] = "WYS File Storage API Service"
#     swag['tags'] = [{
#         "name": "Filestorage",
#         "description": "Methods to save and get files"
#     }]
#     return jsonify(swag)


@app.route('/api/filestorage/save', methods=['POST'])
@requiere_token
def save_file():
    """
        Save a new file
        ---
        tags:
        - "Filestorage"
        produces:
        - "application/json"
        consumes:
        - "multipart/form-data"
        parameters:
        - name: "file"
          in: "formData"
          description: "File to upload"
          required: true
          type: file
    """
    # Check if the post request has the file part
    token = request.headers.get('authorization', None)
    if not token:
        return make_response('No existe Token', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})
    try:
        token=token.split(' ')
        if len(token)>1:
          token=token[1]
        else:
          token=token[0]
        data = jwt.decode(token, llave, algoritmo)
        data = data
    except:
        return make_response('Debe resgistrar su usuario', 401, {'WWW-Authenticate' : 'Basic realm="Se requiere LogIn"'})

    if 'file' not in request.files:
        abort(HTTPStatus.BAD_REQUEST, "No Multipart file found")
    file = request.files['file']

    if file.filename == '':
        flash('No selected File')
        abort(HTTPStatus.NOT_FOUND, "No selected file")
    encoding = "utf-8"
    filename: str = secure_filename(file.filename)
    filename_ext: str = filename + str(datetime.datetime.timestamp(datetime.datetime.utcnow()))
    filename_coded: str = b64.b64encode(filename_ext.encode(encoding)).decode(encoding)
    ext = ""
    if len(filename.split('.')) > 1:
        ext = "." + filename.split('.')[-1]
    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename_coded + ext))
    dict_ret = {'url': FS_DNS_URL + FILE_URL + filename_coded + ext }
    file.close()

    usuario = Usuarios.query.filter_by(id=data['user_id']).first()
    usuario.foto = FS_DNS_URL + FILE_URL + filename_coded + ext
    db.session.commit()

    return jsonify(dict_ret), HTTPStatus.CREATED


@app.route(FILE_URL + '<filename>', methods=['GET'])
def get_file(filename: str):
    """
        Get a file saved in this service
        ---

        tags:
        - "Filestorage"
        produces:
        - "application/json"
        consumes:
        - "multipart/form-data"
        parameters:
        - in: "path"
          name: "filename"
          description: "Filename to download"
          required: true
    """
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)


@app.route(FILE_URL + '<filename>', methods=['PUT'])
@requiere_token
def update_file(filename: str):
    """
        Update a file
        ---

        tags:
        - "Filestorage"
        parameters:
        - in: "path"
          name: "filename"
          description: "Filename to update"
          required: true
        - name: "file"
          in: "formData"
          description: "File to upload"
          required: true
          type: file
        responses:
          200:
            description: Update successfully
          404:
            description: "File not found"
          500:
            description: "Internal Error"

    """
    # Check if file exist
    path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    if not os.path.isfile(path):
        err = {'msg': f"{filename} File not found"}
        logging.warning(err['msg'])
        abort(HTTPStatus.NOT_FOUND, jsonify(err))

    try:
        # Save file
        file = request.files['file']
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        file.close()
        dict_ret = {
            'url': FS_DNS_URL + FILE_URL + filename
        }
        return jsonify(dict_ret)

    except Exception as ex:
        err = {
            'msg': f"Can't update file {filename}",
            'err': ex
        }
        logging.warning(err)
        abort(HTTPStatus.INTERNAL_SERVER_ERROR, jsonify(err))


@app.route(FILE_URL + '<filename>', methods=['DELETE'])
@requiere_token
def delete_file(filename):
    """
    Delete a file
    ---
    tags:
    - "Filestorage"
    parameters:
    - in: "path"
      name: "filename"
      description: "Filename to update"
      required: true
    responses:
      200:
        description: Deleted
      404:
        description: Not found

    """
    path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    if not os.path.isfile(path):
        err = {'msg': f"{filename} File not found"}
        logging.warning(err['msg'])
        abort(HTTPStatus.NOT_FOUND, jsonify(err))

    os.remove(path)
    return jsonify({'msg': "Deleted"}, HTTPStatus.OK)


#################################################################### Iniciar la APP
if __name__ == "__main__":                                      ####
    app.debug=True                                              ####
    app.run(host='0.0.0.0')                                     ####
#################################################################### Fin
