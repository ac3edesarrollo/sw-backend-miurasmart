# -*- coding: utf-8 -*-
"""
Created on Wed Mar 23 13:50:26 2022

@author: Desarrollo
"""

import json
from run import db
from sqlalchemy.ext.declarative import DeclarativeMeta

################################################################## Json Encoder
class AlchemyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
                data = obj.__getattribute__(field)
                try:
                    json.dumps(data) # this will fail on non-encodable values, like other classes
                    fields[field] = data
                except TypeError:
                    fields[field] = None
            # a json-encodable dict
            return fields
        return json.JSONEncoder.default(self, obj)
    
######################################################################## Tablas

class Usuarios(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(80))
    apellido = db.Column(db.String(80))
    direccion = db.Column(db.String(150))
    email = db.Column(db.String(80))
    user_name = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(150))
    perfil = db.Column(db.String(20))
    last_login = db.Column(db.DateTime)
    date_birth = db.Column(db.String(80))
    deleted_on = db.Column(db.DateTime)
    foto = db.Column(db.String(255))
    
    def __init__(self, nombre, apellido, direccion, email, user_name, password, perfil, last_login, date_birth, deleted_on, foto):
        self.nombre = nombre
        self.apellido = apellido
        self.direccion = direccion
        self.email = email
        self.user_name = user_name
        self.password = password
        self.perfil = perfil
        self.last_login = last_login
        self.date_birth = date_birth
        self.deleted_on = deleted_on
        self.foto = foto

    def __str__(self):
        return "{ " + json.dumps(f"id")+": "+json.dumps(self.id)\
                    +", "+json.dumps(f"nombre")+": "+json.dumps(f"{self.nombre}")\
                    +", "+json.dumps(f"apellido")+": "+json.dumps(f"{self.apellido}")\
                    +", "+json.dumps(f"direccion")+": "+json.dumps(f"{self.direccion}")\
                    +", "+json.dumps(f"email")+": "+json.dumps(f"{self.email}")\
                    +", "+json.dumps(f"user_name")+": "+json.dumps(f"{self.user_name}")\
                    +", "+json.dumps(f"password")+": "+json.dumps(f"{self.password}")\
                    +", "+json.dumps(f"perfil")+": "+json.dumps(f"{self.perfil}")\
                    +", "+json.dumps(f"last_login")+": "+json.dumps(f"{self.last_login}")\
                    +", "+json.dumps(f"date_birth")+": "+json.dumps(f"{self.date_birth}")\
                    +", "+json.dumps(f"deleted_on")+": "+json.dumps(f"{self.deleted_on}")\
                    +", "+json.dumps(f"foto")+": "+json.dumps(f"{self.foto}") + " }"

###############################################################################
class Productos(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tipo = db.Column(db.String(80))
    modelo = db.Column(db.String(80))
    imagen = db.Column(db.String(250))
    deleted_on = db.Column(db.DateTime)
    
    def __init__(self, tipo, modelo, imagen, deleted_on):
        self.tipo = tipo
        self.modelo = modelo
        self.imagen = imagen
        self.deleted_on = deleted_on

    def __str__(self):
        return "{ " + json.dumps(f"id")+": "+json.dumps(self.id)\
                    +", "+json.dumps(f"tipo")+": "+json.dumps(f"{self.tipo}")\
                    +", "+json.dumps(f"modelo")+": "+json.dumps(f"{self.modelo}")\
                    +", "+json.dumps(f"imagen")+": "+json.dumps(f"{self.imagen}")\
                    +", "+json.dumps(f"deleted_on")+": "+json.dumps(f"{self.deleted_on}") + " }"

###############################################################################
class Bolsos(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tipo = db.Column(db.String(80))
    modelo = db.Column(db.String(80))
    namebyuser = db.Column(db.String(80))
    product_name = db.Column(db.String(80))
    imagen = db.Column(db.String(80))
    date_create = db.Column(db.DateTime)
    deleted_on = db.Column(db.DateTime)
    user_id = db.Column(db.Integer)
    register_date = db.Column(db.DateTime)
    activo = db.Column(db.String(80))
    mac_address = db.Column(db.String(80))
    
    def __init__(self, tipo, modelo, namebyuser, product_name, imagen, date_create, deleted_on, user_id, register_date, activo, mac_address):
        self.tipo = tipo
        self.modelo = modelo
        self.namebyuser = namebyuser
        self.product_name = product_name
        self.imagen = imagen
        self.date_create = date_create
        self.deleted_on = deleted_on
        self.user_id = user_id
        self.register_date = register_date
        self.activo = activo
        self.mac_address = mac_address

    def __str__(self):
        return "{ " + json.dumps(f"id")+": "+json.dumps(self.id)\
                    +", "+json.dumps(f"tipo")+": "+json.dumps(f"{self.tipo}")\
                    +", "+json.dumps(f"modelo")+": "+json.dumps(f"{self.modelo}")\
                    +", "+json.dumps(f"namebyuser")+": "+json.dumps(f"{self.namebyuser}")\
                    +", "+json.dumps(f"product_name")+": "+json.dumps(f"{self.product_name}")\
                    +", "+json.dumps(f"imagen")+": "+json.dumps(f"{self.imagen}")\
                    +", "+json.dumps(f"date_create")+": "+json.dumps(f"{self.date_create}")\
                    +", "+json.dumps(f"deleted_on")+": "+json.dumps(f"{self.deleted_on}")\
                    +", "+json.dumps(f"user_id")+": "+json.dumps(f"{self.user_id}")\
                    +", "+json.dumps(f"register_date")+": "+json.dumps(f"{self.register_date}")\
                    +", "+json.dumps(f"activo")+": "+json.dumps(f"{self.activo}")\
                    +", "+json.dumps(f"mac_address")+": "+json.dumps(f"{self.mac_address}") + " }"

###############################################################################
class Tipos(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tipo_producto = db.Column(db.String(80))
    position_freq = db.Column(db.Integer)
    date_create = db.Column(db.DateTime)
    deleted_on = db.Column(db.DateTime)
    
    def __init__(self, tipo_producto, position_freq, date_create, deleted_on):
        self.tipo_producto = tipo_producto
        self.position_freq = position_freq
        self.date_create = date_create
        self.deleted_on = deleted_on

    def __str__(self):
        return "{ " + json.dumps(f"id")+": "+json.dumps(self.id)\
                    +", "+json.dumps(f"tipo_producto")+": "+json.dumps(f"{self.tipo_producto}")\
                    +", "+json.dumps(f"position_freq")+": "+json.dumps(f"{self.position_freq}")\
                    +", "+json.dumps(f"date_create")+": "+json.dumps(f"{self.date_create}")\
                    +", "+json.dumps(f"deleted_on")+": "+json.dumps(f"{self.deleted_on}") + " }"

###############################################################################
class Positions(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    time_stamp = db.Column(db.DateTime)
    bolso_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    latitud = db.Column(db.Float)
    longitud = db.Column(db.Float)
    
    def __init__(self, time_stamp, bolso_id, user_id, latitud, longitud):
        self.time_stamp = time_stamp
        self.bolso_id = bolso_id
        self.user_id = user_id
        self.latitud = latitud
        self.longitud = longitud

    def __str__(self):
        return json.dumps({
                    "id" : self.id,
                    "time_stamp" :  str(self.time_stamp),
                    "bolso_id" :  self.bolso_id,
                    "user_id" :  self.user_id,
                    "latitud" :  self.latitud,
                    "longitud" :  self.longitud
                }, sort_keys=True)


        # return "{ " + json.dumps(f"id")+": "+json.dumps(self.id)\
        #             +", "+json.dumps(f"time_stamp")+": "+json.dumps(f"{self.time_stamp}")\
        #             +", "+json.dumps(f"bolso_id")+": "+json.dumps(f"{self.bolso_id}")\
        #             +", "+json.dumps(f"user_id")+": "+json.dumps(f"{self.user_id}")\
        #             +", "+json.dumps(f"latitud")+": "+json.dumps({self.latitud})\
        #             +", "+json.dumps(f"longitud")+": "+json.dumps({self.longitud}) + " }"

###############################################################################
class Alertas(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    time_stamp = db.Column(db.DateTime)
    bolso_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    alert_name = db.Column(db.String(80))
    status = db.Column(db.String(15))
    latitud = db.Column(db.String(80))
    longitud = db.Column(db.String(80))
    
    def __init__(self, time_stamp, bolso_id, user_id, alert_name, status, latitud, longitud):
        self.time_stamp = time_stamp
        self.bolso_id = bolso_id
        self.user_id = user_id
        self.alert_name = alert_name
        self.status = status
        self.latitud = latitud
        self.longitud = longitud

    def __str__(self):
        return "{ " + json.dumps(f"id")+": "+json.dumps(self.id)\
                    +", "+json.dumps(f"time_stamp")+": "+json.dumps(f"{self.time_stamp}")\
                    +", "+json.dumps(f"bolso_id")+": "+json.dumps(f"{self.bolso_id}")\
                    +", "+json.dumps(f"user_id")+": "+json.dumps(f"{self.user_id}")\
                    +", "+json.dumps(f"alert_name")+": "+json.dumps(f"{self.alert_name}")\
                    +", "+json.dumps(f"status")+": "+json.dumps(f"{self.status}")\
                    +", "+json.dumps(f"latitud")+": "+json.dumps(f"{self.latitud}")\
                    +", "+json.dumps(f"longitud")+": "+json.dumps(f"{self.longitud}") + " }"


################################################################## Crear Tablas
with app.app_context():
    db.create_all()